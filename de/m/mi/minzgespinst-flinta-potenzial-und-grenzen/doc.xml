<body>
  <p>Wofür steht FLINTA*?</p>
  <ul>
    <li>
      <p>Frauen</p>
    </li>
    <li>
      <p><a href="https://queer-lexikon.net/2017/06/08/lesbe/">Lesben</a></p>
    </li>
    <li>
      <p><a href="https://queer-lexikon.net/2017/06/08/inter/">Inter*</a></p>
    </li>
    <li>
      <p><a href="https://queer-lexikon.net/2017/06/08/nichtbinaer/">Nichtbinäre Menschen</a></p>
    </li>
    <li>
      <p><a href="https://queer-lexikon.net/2017/06/08/trans/">Trans*</a></p>
    </li>
    <li>
      <p><a href="https://queer-lexikon.net/2017/06/15/agender/">Ageschlechtliche Menschen</a></p>
    </li>
    <li>
      <p>Das Sternchen steht als Platzhalter für ungenannte Identitäten</p>
    </li>
  </ul>
  <heading level="1">
    <title>Warum FLINTA?</title>
  </heading>
  <p>FLINTA* ist ein Akronym<fn id="1"/>, das eine lange, historische Entwicklung hinter sich hat. Ungefähr in den 1970er Jahren entstanden die ersten Frauenräume, um Frauen eine Art Schutzraum vor dem <a href="https://de.wikipedia.org/wiki/Patriarchat_(Soziologie)">Patriarchat</a> zu bieten. Später (in den 80er Jahren) erweiterten sich einige dieser Räume explizit auf FrauenLesben. Das lag unter Anderem am „lesbischen Separatismus“, einer Strömung, die „Frauen“ in Abhängigkeit zum heterosexuellen Mann definierte. Lesben waren somit keine Frauen, sondern Lesben.</p>
  <footnote id="1">
    <p>Akronym = Wort, das aus den Anfangsbuchstaben mehrerer einzelner Wörter zusammengesetzt ist.</p>
  </footnote>
  <p>„Lesbisch“ kann also nicht nur eine sexuell-romantische Orientierung, sondern auch ein (nichtbinäres) Geschlecht sein. In den 90er Jahren kam das T hinzu, um vor allem trans Frauen explizit einzuschließen. Trans Männer waren in vielen Fällen bereits vor ihrer Transition Teil von feministischen Räumen und somit oft automatisch „irgendwie“ dabei.</p>
  <p>Seit den 2010er Jahren wurden dann nichtbinäre und ageschlechtliche Identitäten mitbenannt, auch, um ein Gegengewicht zum Begriff „<a href="https://minzgespinst.net/sternenhimmel/">Frauen*</a>“ zu bilden. Wer mehr über die historischen Entwicklungen lesen möchte, kann das beispielsweise im Spinnboden-Archiv in Berlin tun. Diese Geschichte der Entwicklung von Frauen- zu FLINTA*-Räumen ist wichtig für eine heutige Kritik am Begriff und seiner Verwendung.</p>
  <heading level="1">
    <title>Verwendung und Kritik</title>
  </heading>
  <p>FLINTA* ist ein Überbegriff für Menschen, die in einem <a href="https://www.instagram.com/p/CVlKXnQgZsQ/">endo-cis-sexistischen</a><fn id="2"/> System marginalisiert<fn id="3"/> sind. Der Begriff schließt also auch Männer ein – und zwar nicht nur trans Männer, sondern auch inter cis Männer.</p>
  <footnote id="2">
    <p>Endo-Cis-Sexismus = das System geschlechtlicher und medizinischer Normen, das besonders cis Frauen, inter und trans Menschen schadet</p>
  </footnote>
  <footnote id="3">
    <p>marginalisiert = gesellschaftlich an den Rand gedrängt, unterdrückt</p>
  </footnote>
  <p>Oft sagen oder schreiben Menschen aber „FLINTA*“ und meinen damit „irgendwie weiblich“, „Leute, die ich für Frauen halte“ oder „Menschen, die von den meisten anderen für Frauen gehalten werden“. Manchmal meinen sie auch „Menschen mit Vulva<fn id="4"/>„, „Leute mit Brüsten“ oder „Personen, die menstruieren“. Wir sehen, die Geschichte des Begriffs sitzt tief. Das zeigt sich auch regelmäßig in Veranstaltungseinladungen, die sich an „FLINTA*“ richten, gleichzeitig aber Männer – oder auch „nur“ cis Männer  – explizit ausschließen. Oder wenn in einer Gruppe zu kritischer Männlichkeit die Frage auftaucht, ob Menschen lieber mit Männern oder mit FLINTA* sprechen.</p>
  <footnote id="4">
    <p>Vulva = Venushügel, Schamlippen und Klitoris</p>
  </footnote>
  <p>Viele <a href="https://queer-lexikon.net/2017/06/15/dmab/">AMAB</a> Personen, trans Männer und inter-cis Männer sind damit zwar theoretisch „mitgemeint“, praktisch aber ausgeschlossen. Und ihre Perspektiven finden selten Raum. Es ist nicht ausreichend durchdacht, Menschengruppen nur als Buchstaben in ein Akronym aufzunehmen und es dann „nach Bauchgefühl“ zu verwenden. Frauen* durch FLINTA* zu ersetzen bringt nichts, wenn nicht auch die politische Analyse dahinter weiterentwickelt wird.</p>
  <p>Mittlerweile misstrauen viele „mitgemeinte“ Menschen dem Begriff zu Recht genauso wie der Bezeichnung „Frauen*“. Anstatt immer neue, angeblich inklusive<fn id="5"/> Begriffe zu erschaffen, müssen wir Endo-Cis-Sexismus als Ganzes reflektieren und unsere Art, zu denken, ändern. Zum Beispiel, indem wir Menschen keine bestimmte Sozialisierung aufgrund ihres Geschlechts zuschreiben.</p>
  <footnote id="5">
    <p>inklusiv = miteinbeziehend</p>
  </footnote>
<heading level="1">
  <title>Alternativen</title>
</heading>
<p>Wir müssen uns dringend Gedanken darüber machen, wozu sichere Räume da sein sollen. In einem FLINTA* Raum kann es zu Transfeindlichkeit kommen. Transfeindliche Frauen existieren. Wie soll damit umgegangen werden? Wollen wir über Menstruation reden? Über sexualisierte Gewalt? Soll über Männlichkeit gesprochen werden? Natürlich kann auch über Sozialisierung geredet werden. Oder darüber, was das Patriarchat aus uns gemacht hat. Wenn cis Frauen sich einen Raum für sich wünschen, ist das genauso in Ordnung. Wenn es um Blasenentzündungen geht, sind nicht FLINTA* anfälliger, sondern Menschen mit kurzer Harnröhre.</p>
  <p>Es ist gewaltvoll, Menschen die Zugehörigkeit zur Gruppe abzusprechen. Und es ist problematisch, eine so unterschiedliche Gruppe von Menschen unter einen Begriff zu fassen und zu erwarten, dass alle die gleichen Erfahrungen gemacht haben.</p>
  <p>Um eine nichtbinäre, inter Person, die im Alltag als cis Mann lebt, zu zitieren: „Das letzte Mal Catcalling hab  ich in einer Schwulenbar erlebt. Was soll ich auf ner Demo gegen Catcalling für FLINTA*? Im Aufruf steht, alle FLINTA* erleben Catcalling. Bin ich Ally? Bin ich nicht FLINTA*?“</p>
</body>
